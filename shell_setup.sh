gcloud projects list

project=$(gcloud projects list --filter name='qwiklabs-gcp*' --format='table[no-heading]("PROJECT_ID")')

if [ ! $project ];
then
  echo "ERROR - There appears to be a problem with your Cloud Shell setup. Restart of shell necessary."
  export BADSHELL=TRUE
  exit
fi

gcloud config set project $project 

zone_pick=0

valid_zones=([1]="us-central1-f"  [2]="us-west4-c" [3]="asia-southeast1-c" [4]="asia-southeast2-c")

while [[ ${valid_zones[$zone_pick]} == "" ]]; 
do
  echo "Pick one of the following zones (1-${#valid_zones[*]}):"
  for i in ${!valid_zones[@]}; do
    echo $i " - " ${valid_zones[$i]}
  done
  read zone_pick
if [[ ${valid_zones[$zone_pick]} == "" ]]; then 
  echo "Your choice was not valid, please try again"
fi
done

zone=${valid_zones[$zone_pick]}
region=${zone::-2}
echo "setting default region $region and zone $zone" 

gcloud compute project-info add-metadata \
	    --metadata google-compute-default-region=$region,google-compute-default-zone=$zone
gcloud config set compute/zone $zone

echo "project_id=\""$(gcloud config get-value project)"\"">terraform.tfvars
echo "zone=\""$zone"\"">>terraform.tfvars
echo "region=\""$region"\"">>terraform.tfvars
cp terraform.tfvars lab-setup/.

sed -i "s/ZONE/${zone}/g" app-build/Jenkinsfile app-deploy-dev/Jenkinsfile app-deploy-prod/Jenkinsfile app-infra/Jenkinsfile base-vm/Jenkinsfile
sed -i "s/REGION/${region}/g" app-build/Jenkinsfile app-deploy-dev/Jenkinsfile app-deploy-prod/Jenkinsfile app-infra/Jenkinsfile base-vm/Jenkinsfile
