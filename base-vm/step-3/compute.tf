resource "google_compute_disk" "template-disk" {
 provider = google-beta
 name         = "tomcat9-template-disk"
 image = "debian-cloud/debian-10"
}

resource "google_compute_image" "template-image-1" {
 provider = google-beta
  name = "tomcat-template-image-1"
  source_disk = google_compute_disk.template-disk.name
}

output "tomcat_image_name"{
  value = google_compute_image.template-image-1.name
}
