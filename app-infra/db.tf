resource "google_sql_database_instance" "workshop-db" {
  provider = google-beta
  name             = "workshop-db"
  database_version = "MYSQL_8_0"

  settings {
    # Second-generation instance tiers are based on the machine
    # type. See argument reference below.
    tier = "db-custom-1-3840"
    disk_type = "PD_HDD"
    disk_size = "20"
    availability_type = "ZONAL"
    ip_configuration {
      ipv4_enabled = "true"
      private_network = data.google_compute_network.default.id
    }
  }
  depends_on = [google_service_networking_connection.private_vpc_connection]
  deletion_protection  = "true"
}

resource "random_password" "db-password" {
  length           = 16
  special          = true
}

resource "google_sql_database" "workshop" {
  provider = google-beta
  name     = "workshop"
  instance = google_sql_database_instance.workshop-db.name
}


resource "google_sql_user" "workshop-user" {
  provider = google-beta
  name     = "demo"
  instance = google_sql_database_instance.workshop-db.name
  password = random_password.db-password.result
}

resource "google_compute_global_address" "private_ip_address" {
  provider = google-beta

  name          = "private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = data.google_compute_network.default.id
}

resource "google_service_networking_connection" "private_vpc_connection" {
  provider = google-beta

  network       = data.google_compute_network.default.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}
