resource "google_compute_disk" "jenkins-disk" {
 provider = google-beta
 name         = "jenkins-disk"
 image = "cos-cloud/cos-89-16108-403-15"
 size = "25"
}

resource "google_compute_instance" "jenkins" {
 provider = google-beta
 name         = "jenkins"
 machine_type = "e2-medium"
 tags = ["jenkins"]
 boot_disk {
   auto_delete = false
   source = google_compute_disk.jenkins-disk.name
 }

 metadata_startup_script = "docker run --name jenkins -d -p 8080:8080 -p 50000:50000 rippmn/jenkins-lab:2"

 network_interface {
   network = data.google_compute_network.default.name

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
 service_account {
    email  = google_service_account.automation-infra-sa.email
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_disk" "artifactory-disk" {
 provider = google-beta
 name         = "artifactory-disk"
 image = "cos-cloud/cos-89-16108-403-15"
 size = "25"
}

resource "google_compute_instance" "artifactory" {
 provider = google-beta
 name         = "artifactory"
 machine_type = "e2-medium"
 tags = ["artifactory"]
 boot_disk {
   auto_delete = false
   source = google_compute_disk.artifactory-disk.name
 }

 metadata_startup_script = "docker run --name artifactory -d -p 8081:8081 -p 8082:8082 rippmn/artifactory-lab:2_1"

 network_interface {
   network = data.google_compute_network.default.name

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
 service_account {
    email  = google_service_account.automation-infra-sa.email
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_firewall" "allow-artifactory" {
 provider = google-beta
 name    = "default-allow-artifactory"
 network = "default"
 target_tags = ["artifactory"]
 source_ranges = ["0.0.0.0/0"]

 allow {
   protocol = "tcp"
   ports    = ["8081", "8082"]
 }
}

output "jenkins_ip" {
  value = google_compute_instance.jenkins.network_interface.0.access_config.0.nat_ip
}

output "artifactory_ip" {
  value = google_compute_instance.artifactory.network_interface.0.access_config.0.nat_ip
}


