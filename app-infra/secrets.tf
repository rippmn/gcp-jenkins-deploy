resource "google_secret_manager_secret" "secret-db-host" {
  provider = google-beta
  secret_id = "DB_HOST"
  
  labels = {
    jenkins-credentials-type = "string"
  }

  replication {
    automatic = true  
  }
  depends_on = [google_project_service.secret-man]
}

resource "google_secret_manager_secret_version" "secret-version-db-host" {
  provider = google-beta
  secret = google_secret_manager_secret.secret-db-host.id

  secret_data = google_sql_database_instance.workshop-db.private_ip_address
}

resource "google_secret_manager_secret" "secret-db-user" {
  provider = google-beta
  secret_id = "DB_USER"

  labels = {
    jenkins-credentials-type = "string"
  }

  replication {
    automatic = true      
  }
  depends_on = [google_project_service.secret-man]
}

resource "google_secret_manager_secret_version" "secret-version-db-user" {
  provider = google-beta
  secret = google_secret_manager_secret.secret-db-user.id

  secret_data = google_sql_user.workshop-user.name
}

resource "google_secret_manager_secret" "secret-db-pass" {
  provider = google-beta
  secret_id = "DB_PASS"

  labels = {
    jenkins-credentials-type = "string"
  }

  replication {
    automatic = true  
  }
  depends_on = [google_project_service.secret-man]
}

resource "google_secret_manager_secret_version" "secret-version-db-pass" {
  provider = google-beta
  secret = google_secret_manager_secret.secret-db-pass.id

  secret_data = random_password.db-password.result
}

resource "google_secret_manager_secret" "secret-db-name" {
  provider = google-beta
  secret_id = "DB_NAME"

  labels = {
    jenkins-credentials-type = "string"
  }

  replication {
    automatic = true  
  }
  depends_on = [google_project_service.secret-man]
}

resource "google_secret_manager_secret_version" "secret-version-db-name" {
  provider = google-beta
  secret = google_secret_manager_secret.secret-db-name.id

  secret_data = google_sql_database.workshop.name
}





