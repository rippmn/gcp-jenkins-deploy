resource "google_project_service" "secret-man" {
  provider = google-beta
  service = "secretmanager.googleapis.com"
}

resource "google_project_service" "svc-network" {
  provider = google-beta
  service = "servicenetworking.googleapis.com"
}

