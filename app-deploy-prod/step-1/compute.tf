data "google_service_account" "workshop-app-sa" {
  account_id = "workshop-app-sa"
}

data "google_compute_image" "dev-template-image-1" {
 provider = google-beta
  name = "tomcat-template-image-1"
}

resource "google_compute_disk" "app-template-disk" {
 provider = google-beta
 name         = "app-template-disk"
 image = data.google_compute_image.dev-template-image-1.id
}

resource "google_compute_instance" "app-template" {
 provider = google-beta
 name         = "app-template"
 machine_type = "e2-medium"
 tags = ["app-prod-svr"]
 boot_disk {
   auto_delete = false
   source = google_compute_disk.app-template-disk.name
 }

 network_interface {
   network = data.google_compute_network.default.name

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
 service_account {
    email  = data.google_service_account.workshop-app-sa.email
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_firewall" "app-prod-fw" {
 provider = google-beta
 name    = "default-allow-app-prod-8080"
 network = "default"
 target_tags = ["app-prod-svr"]
 source_ranges = ["0.0.0.0/0"]

 allow {
   protocol = "tcp"
   ports    = ["8080"]
 }
}
