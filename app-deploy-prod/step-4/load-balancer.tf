resource "google_compute_firewall" "gclb-healthz" {
  provider = google-beta
  name    = "default-allow-8080-health-check"
  network = data.google_compute_network.default.name
  target_tags = ["app-prod-svr"]
  source_ranges = ["35.191.0.0/16","130.211.0.0/22"]

  allow {
    protocol = "tcp"
    ports    = ["8080"]
  }
}

resource "google_compute_health_check" "http-health-check" {
  provider = google-beta
  name        = "workshop-app-health-check"

  http_health_check {
    port = "8080"
    request_path = "/healthz"
  }
}

resource "google_compute_backend_service" "workshop-app-backend" {
  provider = google-beta
  name = "workshop-app-backend"
  load_balancing_scheme = "EXTERNAL"
  protocol = "HTTP"
  port_name = "http"
  backend {
    group = google_compute_instance_group_manager.workshop-app-group.instance_group
  }
  health_checks = [google_compute_health_check.http-health-check.id]
}

resource "google_compute_url_map" "workshop-app" {
  provider = google-beta
  name        = "workshop-app"

  default_service = google_compute_backend_service.workshop-app-backend.id
}

resource "google_compute_target_http_proxy" "workshop-app-target-proxy" {
  provider = google-beta
  name        = "workshop-app-target-proxy"
  url_map     = google_compute_url_map.workshop-app.id
}

resource "google_compute_global_forwarding_rule" "workshop-app-frontend" {
  provider = google-beta
  name                  = "workshop-app-frontend"
  load_balancing_scheme = "EXTERNAL"
  target       = google_compute_target_http_proxy.workshop-app-target-proxy.id
  ip_protocol = "TCP"
  port_range = "80"
}
