resource "google_service_account" "workshop-app-sa" {
  provider = google-beta
  account_id   = "workshop-app-sa"
  display_name = "Workshop App Service Account"
}

resource "google_project_iam_binding" "works-app-sa-editor" {
  provider = google-beta
  role    = "roles/editor"

  members = [
    format("%s:%s", "serviceAccount", google_service_account.workshop-app-sa.email )
  ]
}

resource "google_project_iam_binding" "works-app-sa-secrets" {
  provider = google-beta
  role    = "roles/secretmanager.secretAccessor"

  members = [
    format("%s:%s", "serviceAccount", google_service_account.workshop-app-sa.email )
  ]
}
