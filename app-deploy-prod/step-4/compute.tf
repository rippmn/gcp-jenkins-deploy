data "google_service_account" "workshop-app-sa" {
  account_id = "workshop-app-sa"
}

data "google_compute_image" "dev-template-image-1" {
  provider = google-beta
  name = "tomcat-template-image-1"
}

resource "google_compute_disk" "app-template-disk" {
  provider = google-beta
  name         = "app-template-disk"
  image = data.google_compute_image.dev-template-image-1.id
}

resource "google_compute_image" "app-template-image-1" {
  provider = google-beta
  name = "app-template-image-1"
  source_disk = google_compute_disk.app-template-disk.name
}

resource "google_compute_instance_template" "app-template-1" {
  provider = google-beta
  name        = "app-template-1"
  machine_type = "e2-medium"
  tags = ["app-prod-svr"]
  disk {
    source_image = google_compute_image.app-template-image-1.name
  }

  network_interface {
    network = data.google_compute_network.default.name

    access_config {
     // Include this section to give the VM an external ip address
    }
  }

  service_account {
    email  = data.google_service_account.workshop-app-sa.email
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_instance_group_manager" "workshop-app-group" {
  provider = google-beta
  name = "workshop-app-group-1"

  base_instance_name = "workshop-app-group"
  zone = var.zone

  version {
    instance_template  = google_compute_instance_template.app-template-1.id
  }

  named_port {
    name = "http"
    port = "8080"
  }

  target_size  = 2

}
