resource "google_compute_disk" "template-disk" {
 provider = google-beta
 name         = "tomcat9-template-disk"
 image = "debian-cloud/debian-10"
}

resource "google_compute_instance" "template" {
 provider = google-beta
 name         = "tomcat9-template"
 machine_type = "e2-medium"
 tags = ["app-svr"]
 boot_disk {
   auto_delete = false
   source = google_compute_disk.template-disk.name
 }

 network_interface {
   network = data.google_compute_network.default.name

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
 service_account {
    email  = google_service_account.workshop-template-sa.email
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_firewall" "allow-app-8080" {
 provider = google-beta
 name    = "default-allow-app-8080"
 network = "default"
 target_tags = ["app-svr"]
 source_ranges = ["0.0.0.0/0"]

 allow {
   protocol = "tcp"
   ports    = ["8080"]
 }
}
