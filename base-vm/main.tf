variable  "region" {}
variable  "zone" {}
variable  "project_id" {}


terraform {
  backend "gcs" {
    bucket  = "PROJECT-tf-state"
    prefix  = "terraform/state/image-maker"
  }
}

provider "google-beta" {
  region      = var.region
  zone   = var.zone
}

data "google_compute_network" "default" {
  provider = google-beta
  name = "default"
}
