pipeline {
    agent any

    tools {
        terraform 'tf'
        maven 'maven'
    }

    parameters {
        run description: '', filter: 'ALL', name: 'app_build', projectName: 'app-build'
    }

    environment {
        TF_VAR_zone="ZONE"
        TF_VAR_region="REGION"
        ARTIFACTORY_HOST="ARTIFACTORY_IP"
        GOOGLE_PROJECT="""${sh(
                returnStdout: true,
                script: '/usr/bin/gcloud config get-value project'
            )}""".trim()
        TF_VAR_project_id="${GOOGLE_PROJECT}"
    }

    stages {
      stage('git the code'){
	        steps{
	            checkout([$class: 'GitSCM', branches: [[name: '*/master']], extensions: [], userRemoteConfigs: [[url: 'https://gitlab.com/rippmn/gcp-jenkins-deploy']]])
	        }
	    }
      stage('artifactory config'){
          steps{
            rtServer (
              id: "workshop-artifactory",
              url: "http://${ARTIFACTORY_HOST}:8081/artifactory",
              credentialsId: "artifactory"
            )
          }
      }
      stage('get app config') {
            steps {
              dir('app-config'){
                rtDownload (
                  serverId: 'workshop-artifactory',
                  spec: '''{
                        "files": [
                      {
                          "pattern": "workshop-generic/workshop-app/workshop-app-config*.tar.gz",
                          "recursive": "false",
                          "flat" : "true",
                          "build": "${app_build_JOBNAME}/${app_build_NUMBER}"
                      }
                  ]
                  }''')
                sh 'tar xvf workshop-app-config*.tar.gz'
              }
            }
      }
      stage('war artifact download'){
          steps{
            sh 'touch blank.war;rm *.war'
            rtDownload (
              serverId: 'workshop-artifactory',
              spec: '''{
                "files": [
                  {
                      "pattern": "workshop-libs-snapshot-local/com/google/example/workshop/0.0.1-SNAPSHOT/*.war",
                      "recursive": "false",
                      "flat" : "true",
                      "build": "${app_build_JOBNAME}/${app_build_NUMBER}"
                  }
                ]
              }'''
            )
          }
        }
	    stage('sed gcs bucket') {
            steps {
              dir('app-deploy-prod'){
                  sh "sed -i 's/PROJECT/${GOOGLE_PROJECT}/g' main.tf"
              }
            }
        }
        stage('get step-1 infra') {
            steps {
              dir('app-deploy-prod'){
                  sh 'cp step-1/* .'
              }               
            }
        }
        stage('terraform init') {
            steps {
              dir('app-deploy-prod'){
                  sh 'terraform init'
              }               
            }
        }
        stage('terrform step 1') {
            steps {
              dir('app-deploy-prod'){
                  sh 'terraform apply --auto-approve'
              }               
            }
        }
        stage('flyway db migration') {
            steps {
              dir('app-config/workshop-app'){
                withCredentials([string(credentialsId: 'DB_HOST', variable: 'DB_HOST'),
                string(credentialsId: 'DB_NAME', variable: 'DB_NAME'),
                string(credentialsId: 'DB_PASS', variable: 'DB_PASS'),
                string(credentialsId: 'DB_USER', variable: 'DB_USER')]) {
                  sh 'mvn flyway:migrate'
                }
              }
	          }
         }
        stage('ssh keygen') {
            steps {
	            sh 'if [ -f ~/.ssh/google_compute_engine ]; then rm ~/.ssh/google_compute_engine*; fi; ssh-keygen -b 2048 -t rsa -f ~/.ssh/google_compute_engine -q -N \"\" '
	          }
        }
	      stage('copy assets to VM'){
	        steps{
            sh '/usr/bin/gcloud compute scp app-deploy-prod/scripts/app-deploy.sh app-template:. --zone \${TF_VAR_zone}'
            sh '/usr/bin/gcloud compute scp workshop*.war app-template:ROOT.war --zone \${TF_VAR_zone}'
            sh '/usr/bin/gcloud compute scp app-config/tomcat-config/setenv.sh app-template:. --zone \${TF_VAR_zone}'
          }
        }
        stage('deploy app'){
	        steps{
            dir('app-deploy-prod'){
	            sh '/usr/bin/gcloud compute ssh app-template --zone \${TF_VAR_zone} --command=\"./app-deploy.sh\"'
            }
          }
        }
	      stage('clean up'){
          steps{
            sh "rm *.war"
            sh "rm -rf app-config"
            sh 'rm ~/.ssh/google_compute_engine'
          }
        }
        stage('get step-2 infra') {
            steps {
              dir('app-deploy-prod'){
                  sh 'cp step-2/* .'
              }               
            }
        }
        stage('terrform step 2') {
            steps {
              dir('app-deploy-prod'){
                  sh 'terraform apply --auto-approve'
              }               
            }
        }
        stage('get step-3 infra') {
            steps {
              dir('app-deploy-prod'){
                  sh 'cp step-3/* .'
              }               
            }
        }
        stage('terrform step 3') {
            steps {
              dir('app-deploy-prod'){
                  sh 'terraform apply --auto-approve'
              }               
            }
        }
        stage('get step-4 infra') {
            steps {
              dir('app-deploy-prod'){
                  sh 'cp step-4/* .'
              }               
            }
        }
        stage('terrform step 4') {
            steps {
              dir('app-deploy-prod'){
                  sh 'terraform apply --auto-approve'
              }               
            }
        }
        stage('final clean up'){
          steps{
            dir('app-deploy-prod'){
              sh 'rm *.tf'
            }
          }
        }
    }
}