data "google_compute_image" "dev-template-image-1" {
  provider = google-beta
  name = "tomcat-template-image-1"
}

resource "google_compute_disk" "app-template-disk" {
  provider = google-beta
  name         = "app-template-disk"
  image = data.google_compute_image.dev-template-image-1.id
}