resource "google_service_account" "workshop-template-sa" {
  provider = google-beta
  account_id   = "workshop-template-sa"
  display_name = "Workshop App Service Account"
}

resource "google_project_iam_binding" "works-template-sa-editor" {
  provider = google-beta
  role    = "roles/editor"

  members = [
    format("%s:%s", "serviceAccount", google_service_account.workshop-template-sa.email )
  ]
}
