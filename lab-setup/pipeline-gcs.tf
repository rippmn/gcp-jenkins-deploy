resource "google_storage_bucket" "terraform-gcs" {
  provider = google-beta
  name          = format("%s-tf-state", var.project_id)
  location      = var.region
  force_destroy = true

  uniform_bucket_level_access = true
}
