data "google_compute_image" "dev-template-image-1" {
 provider = google-beta
  name = "tomcat-template-image-1"
}

data "google_service_account" "workshop-app-sa" {
  account_id = "workshop-app-sa"
}

resource "google_compute_disk" "app-instance-dev-disk" {
 provider = google-beta
 name         = "app-instance-dev-disk"
 image = data.google_compute_image.dev-template-image-1.id
}

resource "google_compute_instance" "app-instance-dev" {
 provider = google-beta
 name         = "app-instance-dev"
 machine_type = "e2-medium"
 tags = ["app-dev-svr"]
 boot_disk {
   auto_delete = false
   source = google_compute_disk.app-instance-dev-disk.name
 }

 network_interface {
   network = data.google_compute_network.default.name

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
 service_account {
    email  = data.google_service_account.workshop-app-sa.email
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_firewall" "dev-app" {
 provider = google-beta
 name    = "default-allow-dev-app-8080"
 network = "default"
 target_tags = ["app-dev-svr"]
 source_ranges = ["0.0.0.0/0"]

 allow {
   protocol = "tcp"
   ports    = ["8080"]
 }
}
