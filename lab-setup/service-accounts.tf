resource "google_service_account" "automation-infra-sa" {
  provider = google-beta
  account_id   = "automation-infra-sa"
  display_name = "Automation Service Account"
}

resource "google_project_iam_member" "automation-infra-sa" {
  provider = google-beta
  project = var.project_id
  role    = "roles/owner"
  member =   format("%s:%s", "serviceAccount", google_service_account.automation-infra-sa.email )
}
