provider "google-beta" {
  project     = var.project_id
  region      = var.region
  zone   = var.zone
}

data "google_compute_network" "default" {
  provider = google-beta
  name = "default"
}
