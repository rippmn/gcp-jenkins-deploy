resource "google_compute_firewall" "jenkins" {
 provider = google-beta
 name    = "default-allow-jenkins-8080"
 network = "default"
 target_tags = ["jenkins"]
 source_ranges = ["0.0.0.0/0"]

 allow {
   protocol = "tcp"
   ports    = ["8080", "50000"]
 }
}
